#!/usr/bin/env python
# Copyright (C) 2012-2020 Euclid Science Ground Segment
#
# This library is free software; you can redistribute it and/or modify it under the terms of the GNU Lesser General
# Public License as published by the Free Software Foundation; either version 3.0 of the License, or (at your option)
# any later version.
#
# This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
# warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
# details.
#
# You should have received a copy of the GNU Lesser General Public License along with this library; if not, write to
# the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
#
from distutils.core import setup

setup(name='IAL.DRM',
      version='0.5.1',
      description='Euclid IAL Distributed Resource Manager',
      author='Stefano Sartor',
      author_email='sartor@oats.inaf.it',
      packages=['euclid_ial',
                'euclid_ial.drm',
                'euclid_ial.drm.system',
                'euclid_ial.drm.scheduler',
                'euclid_ial.drm.copy_manager'],
      scripts=['scripts/ialdrm_check_job_status',
               'scripts/ialdrm_submit_job',
               'scripts/ialdrm_workdir_cleanup',
               'scripts/ialdrm_configuration_test'],
      classifiers=['Development Status :: 4 - Beta']
     )
