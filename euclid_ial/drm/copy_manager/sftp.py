# Copyright (C) 2012-2020 Euclid Science Ground Segment
#
# This library is free software; you can redistribute it and/or modify it under the terms of the GNU Lesser General
# Public License as published by the Free Software Foundation; either version 3.0 of the License, or (at your option)
# any later version.
#
# This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
# warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
# details.
#
# You should have received a copy of the GNU Lesser General Public License along with this library; if not, write to
# the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
#

import os
import euclid_ial.drm.copy_manager as _cp


###################################################################################################
###################################################################################################
###################################################################################################


class FileStager(_cp.FileStager): 
    def get_utils(self):
        s = '''
####################### STAGE FILES UTILITIES ###############################
host_node="'''+self._conf.get_host_name()+'''"
host_dir="'''+os.path.join(self._conf.get_work_dir_host(),self._work_dir)+'''"
node_dir="'''+os.path.join(self._conf.get_work_dir_node(),self._work_dir)+'''"

stage_in () {
    for f in ${files_in[@]}
    do
        echo "[IAL.DRM],stage_in,$f" >> $IALDRMLOG
        echo "get $f $f" >> stage_in.txt 
    done
    sftp '''+self._conf.get_stage_opt_in()+''' -b stage_in.txt "${host_node}:${host_dir}"
    is_error $? "stage_in $f"   
}

stage_out () {
    for f in ${files_out[@]}
    do
        echo "[IAL.DRM],stage_out,$f" >> $IALDRMLOG
        echo "put $f $f" >> stage_out.txt       
    done
    sftp '''+self._conf.get_stage_opt_out()+''' -b stage_out.txt "${host_node}:${host_dir}"
    is_error $? "stage_out $f"     
}

files_in=(
'''
        for f in self.get_dm_in():
            s+= "  %s\n" % f
        for f in self._ports_in:
            s+= "  %s\n" % f
        for f in self._temp_in:
            s+= "  %s\n" % f
        s+='''
)

out_ports=(
'''
        for f in self._ports_out:
            s+= "  %s\n" % f
        s+=''')

code="
from xml.dom import minidom
from sys import argv

xml = minidom.parse(argv[1])
nameElmCandidates = xml.getElementsByTagName('FileName')
for nameElm in nameElmCandidates:
    if nameElm.parentNode.hasAttribute('filestatus'):
        print(nameElm.firstChild.nodeValue)
"

echo "$code" > IAL.DRM.parse_xml.py

#############################################################################       
'''
        return s
    
    
    def get_stage_in(self):
        return '''
######################### STAGE FILES IN #################################### 
stage_in
'''
            
    def get_stage_out(self):
        s = '''
######################### STAGE FILES OUT ###################################
cd ${node_dir}
files_out=()
for port in ${out_ports[@]}
do
    data_f=`python IAL.DRM.parse_xml.py $port`
    is_error $? "parsing error $port"
    for f in $data_f
    do
        files_out+=("data/$f")
    done
done



files_out+=(
'''
        for f in self._ports_out:
            s+= "  %s\n" % f
        for f in self._temp_out:
            s+= "  %s\n" % f
        s+=''')
        
for f in `ls "'''+self._log_dir+'''"`
do
    files_out+=("'''+self._log_dir+'''/$f")
done

stage_out    
'''
        return s

   
