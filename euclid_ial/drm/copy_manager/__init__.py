# Copyright (C) 2012-2020 Euclid Science Ground Segment
#
# This library is free software; you can redistribute it and/or modify it under the terms of the GNU Lesser General
# Public License as published by the Free Software Foundation; either version 3.0 of the License, or (at your option)
# any later version.
#
# This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
# warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
# details.
#
# You should have received a copy of the GNU Lesser General Public License along with this library; if not, write to
# the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
#

import os
from xml.dom import minidom

    
class FileStager:
    def __init__ (self,conf,job):
        self._conf = conf
        self._work_dir = job.get_work_dir()
        self._log_dir = job.get_log_dir()
        self._ports_in  = job.get_iports()
        self._ports_out = job.get_oports()
        self._temp_in = job.get_files_in()
        self._temp_out = job.get_files_out()
        
    def get_dm_in(self):
        pathv = os.path.join(self._conf.get_work_dir_host(),self._work_dir)
        path = os.path.expandvars(pathv)
         
        names=[]
        for p in self._ports_in:
            xml_path = os.path.join(path,p)
            xml = minidom.parse(xml_path)
            nameElmCandidates = xml.getElementsByTagName('FileName')
            for nameElm in nameElmCandidates:
                if nameElm.parentNode.hasAttribute('filestatus'):
                    names.append('data/'+nameElm.firstChild.nodeValue)
        return names
    
    def get_utils(self):
        return ""
    
    def get_stage_in(self):
        return ""
    
    def get_stage_out(self):
        return ""   
    
__all__=['FileStager']