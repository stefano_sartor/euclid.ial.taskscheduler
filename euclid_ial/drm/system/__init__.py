# Copyright (C) 2012-2020 Euclid Science Ground Segment
#
# This library is free software; you can redistribute it and/or modify it under the terms of the GNU Lesser General
# Public License as published by the Free Software Foundation; either version 3.0 of the License, or (at your option)
# any later version.
#
# This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
# warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
# details.
#
# You should have received a copy of the GNU Lesser General Public License along with this library; if not, write to
# the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
#

import os
import fcntl
import pickle
import csv
import importlib
import logging

try:
    import configparser
except:
    import ConfigParser as configparser
    
ENV_BASE_PATH="IAL_DRM_PATH"

JOB_PENDING = "PENDING"
JOB_QUEUED = "QUEUED"
JOB_EXECUTING = "EXECUTING"
JOB_COMPLETED = "COMPLETED"
JOB_ERROR = "ERROR"
JOB_UNKNOWN = "UNKNOWN"
JOB_HELD = "HELD"
JOB_SUSPENDED = "SUSPENDED"
JOB_ABORTED = "ABORTED"

#EXIT CODES
E_SUCCESS = 0
E_INPUTERROR = 1
E_INVALIDCONFIG = 2
E_IOERROR = 3
E_GENERICERROR = -1

DRMs = ['PBS','LOCAL','SGE','HTCONDOR']
STAGEs = ['CP','SCP','SFTP']

def _get_base_path():
    if os.environ.has_key(ENV_BASE_PATH):
        p = os.environ.get(ENV_BASE_PATH)
        p = os.path.expandvars(p)
        return os.path.expanduser(p)
    else:
        return os.path.expandvars("$HOME/.ial_drm")

def _get_lock_path():
    conf = Config()
    return conf.get_lock_path()

def _job_stat(job_id): #throws IOError, BaseException
    ret_dict={'job_id':job_id,'exitcode':0,'stdout':'','stderr':''}
   
    s = State()
    job = s.find_job(job_id)
    
    if not job:
        ret_dict['status']=JOB_UNKNOWN
    elif job.get('exit_status') is not None:
        ret_dict['job_exitcode']=job.get('exit_status')
        if ret_dict['job_exitcode'] == 0:
            ret_dict['status']=JOB_COMPLETED
        else:
            ret_dict['status']=JOB_ERROR
    else:
        ret_dict['status']=JOB_ABORTED
    
    return ret_dict
    
    

class _Lock(object):
    def __init__(self,path): #throws IOError
        #can raise IOError
        self._lock_file = os.open(path, os.O_RDWR | os.O_CREAT)
        #can raise IOError
        fcntl.lockf(self._lock_file, fcntl.LOCK_EX)
        
    def __del__(self): #throws IOError
        #can raise IOError
        fcntl.lockf(self._lock_file, fcntl.LOCK_UN) 
        os.close(self._lock_file)



class Job(object):
    def __init__(self):
        self._opt={'executables':[],'stagein':[],'stageout':[],'iports':[],'oports':[]}
        self._work_dir = ''
        self._log_dir = ''
        self._dirs = []

        self._status=None 
        self.drm_opt={}             
        
    def add_dir(self,dir):
        self._dirs.append(dir)
    
    def get_dirs(self):
        return self._dirs   
    
    def add_iport(self,port):
        self._opt['iports'].append(port) 
     
    def add_oport(self,port):
        self._opt['oports'].append(port) 
        
    def get_iports(self):
        return self._opt['iports']
         
    def get_oports(self):
        return self._opt['oports']
              
    def add_file_in(self,rel_path):
        self._opt['stagein'].append(rel_path)
    
    def get_files_in(self):
        return self._opt['stagein']
    
    def add_file_out(self,rel_path):
        self._opt['stageout'].append(rel_path)
     
    def get_files_out(self):
        return self._opt['stageout']
        
    def add_executable(self,task):
        self._opt['executables'].append(task)
        
    def get_executables(self):
        return self._opt['executables']
        
    def set_work_dir(self,path):
        self._work_dir=path

    def get_work_dir(self):
        return self._work_dir
         
    def set_log_dir(self,path):
        self.add_dir(path)
        self._log_dir=path

    def get_log_dir(self):
        return self._log_dir
    
    def get_id(self):
        if self._status and self._status.has_key('job_id'):
            return self._status['job_id']
        else:
            return None

    def submit(self,opt={}):
        conf = Config()
        drm = conf.get_DrmConnector()
        self._status = drm.submit(self,opt)
#        print (self._status) #DBG
        if self._status['exitcode'] != 0:
            raise SubmissionError(self._status['exitcode'])
        
    def check(self):
        if self._status is None:
            return JOB_UNKNOWN
        conf = Config()
        drm = conf.get_DrmConnector()
        d = drm.stat(self._status['job_id'])
        return d['status']    
    
    def exit_status(self):
        conf = Config()
        drm = conf.get_DrmConnector()
        d = drm.stat(self._status['job_id'])
        if d['status'] == JOB_COMPLETED:
            return d['job_exitcode']
        else:
            raise JobRunningError()

             
class State:
    
    def __init__(self):
        self._log = logging.getLogger(__name__)
        self._jobs = {}
        self._work_dirs = {}
        self._jobs_path = os.path.expandvars(os.path.join(_get_base_path(), 'jobs'))
        self._work_dirs_path = os.path.expandvars(os.path.join(_get_base_path(), 'work_dir'))
#        self._lock_path = os.path.expandvars(os.path.join(_get_base_path(), 'jobs_lock'))
        self._lock_path = _get_lock_path()
        
        
    def _load_jobs(self,lock=None): #throws IOError, BaseException
        if not lock:       
            lock = _Lock(self._lock_path)
        
        if os.path.exists(self._jobs_path):
            #cat raise IOError
            with open(self._jobs_path, 'r+b') as f:
                #can raise IOError and many other exception types.
                self._jobs = pickle.load(f)
        
        if os.path.exists(self._work_dirs_path): #throws IOError, BaseException
            #cat raise IOError
            with open(self._work_dirs_path, 'r+b') as f:
                #can raise IOError and many other exception types.
                self._work_dirs = pickle.load(f)
                
    def _save_jobs(self,lock=None): #throws IOError, BaseException
        if not lock:       
            lock = _Lock(self._lock_path)
        with  open(self._jobs_path, 'w+b') as f:
            pickle.dump(self._jobs, f, protocol=pickle.HIGHEST_PROTOCOL)
            
        with  open(self._work_dirs_path, 'w+b') as f:
            pickle.dump(self._work_dirs, f, protocol=pickle.HIGHEST_PROTOCOL) 
            
    def _find_status(self,path_name): #throws IOError
        status = None
        path = os.path.expandvars(path_name)
        if os.path.isfile(path):
            with open(path,'rt') as f:
                for line in f:
                    if line.find('[IAL.DRM]') == 0:
                        s = line.strip()
                        lst = s.split(',')
                        if lst[1] == 'status':
                            status = int(lst[2])
        else:
            self._log.error("log file not present '%s'" % str(path))
        return status        
        
            
    def add_job(self, job_id, info): #throws: IOError, BaseException
        lock = _Lock(self._lock_path)
        self._load_jobs(lock)
        self._jobs[job_id] = info
        work_dir = info.get('work_dir')
        if work_dir is not None:
            wdids=self._work_dirs.get(work_dir,[])
            wdids.append(job_id)
            self._work_dirs[work_dir]=wdids
        self._save_jobs(lock)
        self._log.debug("new job. id:%s = %s" % (job_id,str(info)))
        
    def delete_job(self,job_id): #throws: IOError, BaseException
        lock = _Lock(self._lock_path)
        self._load_jobs(lock)
        self._jobs.pop(job_id,None)
        self._save_jobs(lock)
        self._log.debug("deleted job. id:%s" % job_id)
        
        
    def delete_jobs_work_dir(self,work_dir): #throws: IOError, BaseException
        lock = _Lock(self._lock_path)
        self._load_jobs(lock)
        id_list = self._work_dirs.pop(work_dir,[])
        for job_id in id_list:
            job = self._jobs.pop(job_id,None)
            if job and job.get('scheduler_log_file'):
                try:
                    os.remove(job.get('jdl'))
                except OSError as e:
                    self._log.error("deleting job. OSError: %s" % str(e))
                    continue
        self._save_jobs(lock)
        
    def find_job(self,job_id): #trows: IOError, BaseException
        self._load_jobs()
        job = self._jobs.get(job_id)
        if job and job.get('scheduler_log_file') and job.get('exit_status') is None:
            status = self._find_status(job.get('scheduler_log_file'))
            if status is not None:
                job['exit_status'] = status
                lock = _Lock(self._lock_path)
                self._load_jobs(lock)
                self._jobs[job_id]=job
                self._save_jobs(lock)
                
        return job
    
    def find_work_dir_jobs(self,work_dir): #throws: IOError, BaseException
        self._load_jobs()
        return list(self._work_dirs.get(work_dir,[]))
               


class Config:
    __config = None
    def __init__(self): #throws InvalidConfigError
        if Config.__config is None:
            self._base_path =  _get_base_path()       
            vpath = os.path.join(_get_base_path(),"conf.ini")
            path = os.path.expandvars(vpath)

            if not os.access(path, os.R_OK):
                raise InvalidConfigError("no readable configuration file: "+path)            

            Config.__config = configparser.ConfigParser()            
            Config.__config.read(path)
            self._config = Config.__config
            self.__validate()
            log_dir= self.get_log_dir()
            if not os.path.exists(log_dir):
                os.makedirs(log_dir)
            log_path = os.path.join(log_dir,"ialdrm.log")
            log_format = '''%(asctime)s %(levelname)s %(name)s:%(message)s'''
            if self._config.has_option('SYSTEM','log_level'):
                loglevel = self._config.get('SYSTEM','log_level')
                loglevel = loglevel.upper()
            else:
                loglevel = 'ERROR'
            
            logging.basicConfig(format=log_format,filename=log_path,level=getattr(logging,loglevel))
                      
        else:
            self._config = Config.__config
        
    def __validate_drm_section(self,drm):
        module = importlib.import_module("euclid_ial.drm.scheduler."+drm.lower())
        if self._config.has_section(drm):
            for (k,_) in self._config.items(drm):
                if k == "extra_commands_before" or k == "extra_commands_after":
                    continue
                if k not in module.DIRECTIVES.keys():
                    raise InvalidConfigError("invalid option '"+k+"' in section "+drm)
                
    def __validate_system_section(self):
        opt = ['drm','work_dir_host','work_dir_node', 'stage_files', 'log_dir' , 'log_level' , 'lock_path']
        for (k,_) in self._config.items('SYSTEM'):
            if k not in opt:
                raise InvalidConfigError("invalid option '"+k+"' in section SYSTEM")
        if self._config.has_option('SYSTEM','log_level'):
            log_level = self._config.get('SYSTEM','log_level')
            log_level = log_level.upper()
            if log_level not in ['NOTSET','INFO','WARN','WARNING','ERROR','CRITICAL','DEBUG']:
                raise InvalidConfigError("wrong log_level value: "+log_level)
                
    def __validate(self): #throws InvalidConfigError
        req_opt= ['drm','work_dir_host','work_dir_node']
        for opt in req_opt:
            if not self._config.has_option('SYSTEM', opt):
                raise InvalidConfigError("missing config option: "+opt)
        
        self.__validate_system_section()
        drm = self._config.get('SYSTEM', 'drm')

        if drm not in DRMs:
            raise InvalidConfigError("DRM not supported: "+drm) 
        self.__validate_drm_section(drm)
        if self._config.has_option('SYSTEM', 'stage_files'):
            stage = self._config.get('SYSTEM', 'stage_files')
            if stage not in STAGEs:
                raise InvalidConfigError("transfer protocol not supported: "+stage)
            if stage != 'CP':
                if not self._config.has_section(stage):
                    raise InvalidConfigError("missing section "+stage)
                if  not self._config.has_option(stage, 'host_name'):
                    raise InvalidConfigError("missing option host_name in section "+stage)
            
        wd_host = os.path.expandvars(self.get_work_dir_host())
        if not os.access(wd_host, os.R_OK and os.W_OK and os.X_OK):
            raise InvalidConfigError("insufficient permission on host dir: "+wd_host)        

    def get_lock_path(self):
        if self._config.has_option('SYSTEM','lock_path'):
            p = self._config.get('SYSTEM','lock_path')
            p = os.path.expandvars(p)
            return os.path.expanduser(p)
        else:
            return '/tmp/ial_drm-lockfile'
        
    def get_DrmConnector(self): #throws: ImportError
        drm = self._config.get('SYSTEM','drm')
        mod_name =   "euclid_ial.drm.scheduler."+drm.lower()
        #can raise ImportError  
        module = importlib.import_module(mod_name)

        return module.DrmConnector()
    
    def get_base_path(self):
        if self._config.has_option('SYSTEM', 'stage_files'):
            cpman = self._config.get('SYSTEM', 'stage_files')
            if cpman == 'CP' and self._config.has_option(cpman, 'base_dir'):
                return self._config.get(cpman, 'base_dir')
        return self.get_work_dir_host()
    
    def get_stage_opt_in(self):
        if self._config.has_option('SYSTEM', 'stage_files'):
            cpman = self._config.get('SYSTEM', 'stage_files')
            if self._config.has_option(cpman, 'options_in'):
                return self._config.get(cpman, 'options_in')
        return ''
    
    def get_stage_opt_out(self):
        if self._config.has_option('SYSTEM', 'stage_files'):
            cpman = self._config.get('SYSTEM', 'stage_files')
            if self._config.has_option(cpman, 'options_out'):
                return self._config.get(cpman, 'options_out')
        return ''
            
    
    def get_FileStager(self,job): #throws ImportError
        if not self.is_stage_files():
            module = importlib.import_module("euclid_ial.drm.copy_manager")
        else:
            cpman = self._config.get('SYSTEM','stage_files')
            mod_name =   "euclid_ial.drm.copy_manager."+cpman.lower()          
            module = importlib.import_module(mod_name)
            
        return module.FileStager(self,job)
        
    def get_log_dir(self):
        if self._config.has_option('SYSTEM', 'log_dir'):
            return self._config.get('SYSTEM','log_dir')
        else:
            return os.path.join(_get_base_path(),"log")
            
    def is_stage_files(self):
        return self._config.has_option('SYSTEM', 'stage_files')
             
    def get_work_dir_host(self):
        return self._config.get('SYSTEM','work_dir_host')
         
    def get_work_dir_node(self):
        return self._config.get('SYSTEM','work_dir_node')
    
    def get_host_name(self):
        if self.is_stage_files():
            stage_sect = self._config.get('SYSTEM','stage_files')
            if stage_sect != 'CP':
                return self._config.get(stage_sect,'host_name')
            else:
                return ""
        else:
            return ""
    
    def get_extra_commands_before(self):
        drm = self._config.get('SYSTEM','drm')
        if self._config.has_section(drm):
            if self._config.has_option(drm,"extra_commands_before"):
                return self._config.get(drm,"extra_commands_before")
        return ""
     
    def get_extra_commands_after(self):
        drm = self._config.get('SYSTEM','drm')
        if self._config.has_section(drm):
            if self._config.has_option(drm,"extra_commands_after"):
                return self._config.get(drm,"extra_commands_after")
        return ""
                  
    def get_drm_options(self): #throws ImportError
        opt = {}
        drm = self._config.get('SYSTEM','drm')
        module = importlib.import_module("euclid_ial.drm.scheduler."+drm.lower())
        if self._config.has_section(drm):
            for (k,i) in self._config.items(drm):
                if k in module.DIRECTIVES.keys():
                    opt[k]=i
        opt.pop("extra_commands_before",'')
        opt.pop("extra_commands_after",'')       
            
        return opt

class InvalidConfigError(Exception):
    def __init__(self,s=""):
        self.msg=s
    def __str__(self):
        return "[InvalidConfigurationError] " + self.msg
    
    
class SubmissionError(Exception):
    def __init__(self,s=-1):
        self.msg=s
    def __str__(self):
        return "qsub returned " + self.msg      
      
class JobRunningError(Exception):
    def __str__(self):
        return "cannot return exit status of a running job"  

        
__all__=['Config','State','Job']
