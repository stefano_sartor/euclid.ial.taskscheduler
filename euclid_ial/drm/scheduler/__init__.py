# Copyright (C) 2012-2020 Euclid Science Ground Segment
#
# This library is free software; you can redistribute it and/or modify it under the terms of the GNU Lesser General
# Public License as published by the Free Software Foundation; either version 3.0 of the License, or (at your option)
# any later version.
#
# This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
# warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
# details.
#
# You should have received a copy of the GNU Lesser General Public License along with this library; if not, write to
# the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
#

import os
import uuid
import euclid_ial.drm.system as system

class DrmConnector:
    def __init__(self):
        self._uid = str(uuid.uuid1().hex)
        self._log = "ialdrm."+self._uid+'.log'
    
    def submit(self, job, opt_override={}):
        pass
    
    def stat(self, job_id):
        pass
    
    def delete(self, job_id):
        pass
    
    def _prapare_host(self,job):
        conf = system.Config()
        host_base_dir = os.path.join(conf.get_work_dir_host(), job.get_work_dir())    
        for d in job.get_dirs(): 
            path = os.path.join(host_base_dir,d)
            if not os.path.exists(path):
                os.makedirs(path)
    
    def _get_utils(self,log_dir):
        p = os.path.join(log_dir,self._log)
        return '''
IALDRMLOG="'''+p+'''"
   
print_status () {
    echo "[IAL.DRM],status,$1" >> $IALDRMLOG
    if [ $1 -ne 0 ]
    then
        echo "[IAL.DRM],$2,$1" >> $IALDRMLOG
        exit $1
    fi
}

is_error () {
    if [ $1 -ne 0 ]
    then
        echo "[IAL.DRM],$2,$1"    >> $IALDRMLOG     
        echo "[IAL.DRM],status,$1" >> $IALDRMLOG
        exit $1
    fi
}
'''
    
__all__=['DrmConnector','local','pbs','sge','htcondor']