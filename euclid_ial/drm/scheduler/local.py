# Copyright (C) 2012-2020 Euclid Science Ground Segment
#
# This library is free software; you can redistribute it and/or modify it under the terms of the GNU Lesser General
# Public License as published by the Free Software Foundation; either version 3.0 of the License, or (at your option)
# any later version.
#
# This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
# warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
# details.
#
# You should have received a copy of the GNU Lesser General Public License along with this library; if not, write to
# the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
#
import os
import stat
import uuid
import datetime
import subprocess

import euclid_ial.drm.system as system
import euclid_ial.drm.scheduler as _drm

HPC_extra_before  = "extra_commands_before"
HPC_extra_after  = "extra_commands_after"


DIRECTIVES = {}

class DrmConnector(_drm.DrmConnector):

 
    def submit(self, job, opt_override={}):
        ret_dict={}
        name = job.get_executables()[0].split()[0]
        conf = system.Config()
        directives={}
        directives.update(conf.get_drm_options())
        directives.update(job.drm_opt)
        directives.update(opt_override)
        
        
        uid = str(uuid.uuid1().hex)
        
        jdl_dir = conf.get_log_dir()
        jdl_path = os.path.join(jdl_dir,uid + '.jdl')
         
        
        host_log_dir = os.path.join(conf.get_work_dir_host(), job.get_work_dir(),job.get_log_dir())
        node_log_dir = os.path.join(conf.get_work_dir_node(), job.get_work_dir(),job.get_log_dir())

        self._prapare_host(job)
        
        copy_files_utils = ''
        copy_files_in = ''
        copy_files_out = ''
        if conf.is_stage_files():
            cpman = conf.get_FileStager(job)
            copy_files_utils = cpman.get_utils()
            copy_files_in = cpman.get_stage_in()
            copy_files_out = cpman.get_stage_out()
        
        out_file=os.path.join(host_log_dir,name+'.out')        
        err_file=os.path.join(host_log_dir,name+'.err')

        with open(jdl_path,'w') as jdl:
            jdl.write('#!/bin/bash\n')
            for key in directives.keys():
                jdl.write("%s %s\n" % (DIRECTIVES[key],  directives[key]))
            
            work_dir = os.path.join(conf.get_work_dir_node(),job.get_work_dir())
            jdl.write('mkdir -p %s\n' % work_dir)
            jdl.write('cd %s\n' % work_dir)
            
            for d in job.get_dirs():
                jdl.write('mkdir -p %s\n' % d)
             
            jdl.write(self._get_utils(node_log_dir))                       
            jdl.write(copy_files_utils)
            jdl.write(copy_files_in)
            
            jdl.write('%s\n' % conf.get_extra_commands_before())    
            for task in job.get_executables():
                jdl.write('%s\n' % task)
                jdl.write('print_status $? "task_error"\n')
            jdl.write('%s\n' % conf.get_extra_commands_after())                

            jdl.write(copy_files_out)

        os.chmod(jdl_path, stat.S_IRWXU | stat.S_IRGRP | stat.S_IXGRP)
        
        command = jdl_path

        # launch the job
        pid = subprocess.Popen(command, shell=True, stdout = open(out_file,'w'),\
                               stderr = open(err_file,'w'), ).pid
              
        ret_dict['stdout']=''
        ret_dict['stderr']='' 
        ret_dict['exitcode']=0
        
        jobid = str(pid)
        ret_dict['job_id']=jobid
        
        j = {}
        j['jdl'] = jdl_path
        j['submit_date'] = str(datetime.datetime.now())
        j['uid'] = self._uid
        j['scheduler_log_file'] =  os.path.join(host_log_dir,self._log)
        j['work_dir']=job.get_work_dir()
        state = system.State()
        state.add_job(jobid, j)
        return ret_dict
        
        
        
    
    def stat(self, job_id):
        ret_dict={'job_id':job_id}
        cmd = "ps -el | grep %s" % job_id
        proc = subprocess.Popen(cmd, stdout = subprocess.PIPE, \
                                stderr = subprocess.STDOUT, shell = True, close_fds = True)
        
        (stdout, stderr) = proc.communicate()
        if stdout is None:
            stdout = ''
        if stderr is None:
            stderr = ''
        
        ret_dict['stdout']=stdout
        ret_dict['stderr']=stderr 
        ret_dict['exitcode']=proc.returncode
                   
        if proc.returncode != 0:
            return system._job_stat(job_id)
        

        
        if stdout != '':
            ret_dict['status']= system.JOB_EXECUTING
            return  ret_dict
        else:
            return system._job_stat(job_id)

    
    def delete(self, job_id):
        #TODO
        pass
    