# Copyright (C) 2012-2020 Euclid Science Ground Segment
#
# This library is free software; you can redistribute it and/or modify it under the terms of the GNU Lesser General
# Public License as published by the Free Software Foundation; either version 3.0 of the License, or (at your option)
# any later version.
#
# This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
# warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
# details.
#
# You should have received a copy of the GNU Lesser General Public License along with this library; if not, write to
# the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
#

import os
import datetime
import subprocess
import logging

import euclid_ial.drm.system as system
import euclid_ial.drm.scheduler as _drm

# SGE directives
SGE_a = "execution_time"
SGE_c = "checkpoint"
SGE_e = "error_path"
SGE_f = "fault_tolerant"
SGE_h = "hold_types"
SGE_j = "join_path"
SGE_l = "resource_list"
SGE_m = "mail_points"
SGE_o = "output_path"
SGE_p = "priority"
SGE_q = "destination"
SGE_r = "rerunable"
SGE_t = "job_array_request"
SGE_v = "variable_list"
SGE_A = "account_name"
SGE_M = "mail_users"
SGE_N = "job_name"
SGE_S = "shell_path_list"

HPC_extra_before  = "extra_commands_before"
HPC_extra_after  = "extra_commands_after"

DIRECTIVES = {"execution_time": "#$ -a",
        "checkpoint":  "#$ -c",
        "error_path":  "#$ -e",
        "fault_tolerant":  "#$ -f",
        "hold_types":  "#$ -h",
        "join_path":  "#$ -j",
        "resource_list" :  "#$ -l",
        "mail_points":  "#$ -m",
        "output_path":  "#$ -o",
        "priority":  "#$ -p",
        "destination":  "#$ -q",
        "rerunable":  "#$ -r",
        "job_array_request" :  "#$ -t",
        "variable_list" :  "#$ -v",
        "account_name"  :  "#$ -A",
        "mail_users":  "#$ -M",
        "job_name":  "#$ -N",
        "shell_path_list"   :  "#$ -S"
        }

class DrmConnector(_drm.DrmConnector):

    
    def submit(self, job, opt_override={}):
        ret_dict={}
        name = job.get_tasks()[0].split()[0]
        conf = system.Config()
        directives={SGE_N:name}
        directives.update(conf.get_drm_options())
        directives.update(opt_override)
        
        
        jdl_dir = conf.get_log_dir()
        jdl_path = os.path.join(jdl_dir,self._uid + '.jdl')

        host_log_dir = os.path.join(conf.get_work_dir_host(), job.get_work_dir(),job.get_log_dir())
        node_log_dir = os.path.join(conf.get_work_dir_node(), job.get_work_dir(),job.get_log_dir())    

        self._prapare_host(job)
        
        copy_files_utils = ''
        copy_files_in = ''
        copy_files_out = ''
        if conf.is_stage_files():
            cpman = conf.get_FileStager(job)
            copy_files_utils = cpman.get_utils()
            copy_files_in = cpman.get_stage_in()
            copy_files_out = cpman.get_stage_out() 
        
        directives[SGE_o]=os.path.join(host_log_dir,name+'.out')        
        directives[SGE_e]=os.path.join(host_log_dir,name+'.err')
        
        with open(jdl_path,'w') as jdl:
            jdl.write('#!/bin/bash\n')
            for key in directives.keys():
                jdl.write("%s %s\n" % (DIRECTIVES[key],  directives[key]))
            
            work_dir = os.path.join(conf.get_work_dir_node(),job.get_work_dir())
            jdl.write('mkdir -p %s\n' % work_dir)
            jdl.write('cd %s\n' % work_dir)
            
            for d in job.get_dirs():
                jdl.write('mkdir -p %s\n' % d)
                        
             
            jdl.write(self._get_utils(node_log_dir))              
            jdl.write(copy_files_utils)
            jdl.write(copy_files_in)
            
            jdl.write('%s\n' % conf.get_extra_commands_before())    
            for task in job.get_tasks():
                jdl.write(task+'\n')
                jdl.write('print_status $? "task_error"\n')
            jdl.write('%s\n' % conf.get_extra_commands_after())                
        
            jdl.write(copy_files_out) 


        # launch the job
        comm = 'condor_qsub ' + jdl_path
        proc = subprocess.Popen(comm, stdout = subprocess.PIPE, \
        stderr = subprocess.PIPE, shell = True, close_fds = True)
        (stdoutdata, stderrdata) = proc.communicate()

        
        ret_dict['stdout']=stdoutdata
        ret_dict['stderr']=stderrdata 
        ret_dict['exitcode']=proc.returncode
        if proc.returncode != 0:
            ret_dict['job_id']='None'
            return ret_dict
        
        jobid = stdoutdata.split()[2]
        ret_dict['job_id']=jobid
        
        jdl_jobid_path = os.path.join(jdl_dir,jobid+'.jdl')
        os.rename(jdl_path, jdl_jobid_path)
        j = {}
        j['jdl'] = jdl_jobid_path
        j['submit_date'] = str(datetime.datetime.now())
        j['uid'] = self._uid
        j['scheduler_log_file'] =  os.path.join(host_log_dir,self._log)
        j['work_dir']=job.get_work_dir()
        state = system.State()
        state.add_job(jobid, j)
        return ret_dict
        
        
        
    
    def stat(self, job_id):
        # H = on hold,
        # R = running,
        # I = idle (waiting for a machine to execute on),
        # C = completed,
        # X = removed,
        # < = transferring input (or queued to do so),
        # > = transferring output (or queued to do so)

        ret_dict={'job_id':job_id}
        
        proc = subprocess.Popen('condor_q %s' % str(job_id), stdout = subprocess.PIPE, stderr = subprocess.PIPE,
                                shell = True, close_fds = True)
        

        (stdout, stderr) = proc.communicate()
        
        ret_dict['stdout']=stdout
        ret_dict['stderr']=stderr 
        ret_dict['exitcode']=proc.returncode        

        if proc.returncode != 0:
            #ret_dict['status']=system.JOB_UNKNOWN
            #return ret_dict
            return system._job_stat(job_id)


        lines = stdout.split('\n')
        header = True
        for line in lines:
            args = line.split()
            if header :
                if args and args[0] == 'ID':
                    header = False
            elif args :
                job = args[0].split('.')
                if job[0] == job_id:
                    status = args[5]
                    if status == 'H':
                        ret_dict['status']=  system.JOB_HELD
                    elif status == 'R':
                        ret_dict['status']=  system.JOB_EXECUTING
                    elif status == 'I':
                        ret_dict['status']=  system.JOB_QUEUED
                    elif status == 'C':
                        ret_dict['status']=  system.JOB_COMPLETED
                        return system._job_stat(job_id)
                    elif status == 'X':
                        ret_dict['status']=  system.JOB_ABORTED
                    elif status == '<':
                        ret_dict['status']=  system.JOB_SUSPENDED
                    elif status == '>':
                        ret_dict['status']=  system.JOB_SUSPENDED
                    else:
                        return system._job_stat(job_id)
                    return  ret_dict
            
        else:           
            return system._job_stat(job_id) 
    
    def delete(self, job_id):
        #TODO
        pass
    