# Copyright (C) 2012-2020 Euclid Science Ground Segment
#
# This library is free software; you can redistribute it and/or modify it under the terms of the GNU Lesser General
# Public License as published by the Free Software Foundation; either version 3.0 of the License, or (at your option)
# any later version.
#
# This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
# warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
# details.
#
# You should have received a copy of the GNU Lesser General Public License along with this library; if not, write to
# the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
#

import os
import uuid
import datetime
import subprocess

import euclid_ial.drm.system as system
import euclid_ial.drm.scheduler as _drm


# PBS directives
PBS_a = "execution_time"
PBS_c = "checkpoint"
PBS_e = "error_path"
PBS_f = "fault_tolerant"
PBS_g = "group_list"
PBS_h = "hold_types"
PBS_j = "join_path"
PBS_k = "keep_files"
PBS_l = "resource_list"
PBS_m = "mail_points"
PBS_o = "output_path"
PBS_p = "priority"
PBS_q = "destination"
PBS_r = "rerunable"
PBS_t = "job_array_request"
PBS_array_id = "job_array_id"
PBS_u = "user_list"
PBS_v = "variable_list"
PBS_A = "account_name"
PBS_M = "mail_users"
PBS_N = "job_name"
PBS_S = "shell_path_list"
PBS_depend   = "depend"
PBS_inter= "interactive"
PBS_stagein  = "stagein"
PBS_stageout = "stageout"
PBS_jobtype  = "jobtype"

HPC_extra_before  = "extra_commands_before"
HPC_extra_after  = "extra_commands_after"

DIRECTIVES = {"execution_time": "#PBS -a",
        "checkpoint":  "#PBS -c",
        "error_path":  "#PBS -e",
        "fault_tolerant":  "#PBS -f",
        "group_list":  "#PBS -g",
        "hold_types":  "#PBS -h",
        "join_path":  "#PBS -j",
        "keep_files":  "#PBS -k",
        "resource_list" :  "#PBS -l",
        "mail_points":  "#PBS -m",
        "output_path":  "#PBS -o",
        "priority":  "#PBS -p",
        "destination":  "#PBS -q",
        "rerunable":  "#PBS -r",
        "job_array_request" :  "#PBS -t",
        "job_array_id"  : "#PBS -array_id",
        "user_list":  "#PBS -u",
        "variable_list" :  "#PBS -v",
        "account_name"  :  "#PBS -A",
        "mail_users":  "#PBS -M",
        "job_name":  "#PBS -N",
        "shell_path_list"   :  "#PBS -S",
        "depend"  : "#PBS -depend",
        "interactive" : "#PBS -inter",
        "stagein": "#PBS -stagein",
        "stageout" : "#PBS -stageout",
        "jobtype": "#PBS -jobtype"
        }

class DrmConnector(_drm.DrmConnector):
      
    def submit(self, job, opt_override={}):
        ret_dict={}
        name = job.get_executables()[0].split()[0]
        conf = system.Config()
        directives={PBS_N:name}
        directives.update(conf.get_drm_options())
        directives.update(job.drm_opt)
        directives.update(opt_override)
        name = directives[PBS_N]
        
        uid = str(uuid.uuid1().hex)
        
        jdl_dir = conf.get_log_dir()
        jdl_path = os.path.join(jdl_dir,uid + '.jdl')
         
        host_log_dir = os.path.join(conf.get_work_dir_host(), job.get_work_dir(),job.get_log_dir())
        node_log_dir = os.path.join(conf.get_work_dir_node(), job.get_work_dir(),job.get_log_dir())

        self._prapare_host(job)

        
        copy_files_utils = ''
        copy_files_in = ''
        copy_files_out = ''
        if conf.is_stage_files():
            cpman = conf.get_FileStager(job)
            copy_files_utils = cpman.get_utils()
            copy_files_in = cpman.get_stage_in()
            copy_files_out = cpman.get_stage_out()
            
        
        directives[PBS_o]=os.path.join(host_log_dir,name+'.out')        
        directives[PBS_e]=os.path.join(host_log_dir,name+'.err')

        with open(jdl_path,'w') as jdl:
            jdl.write('#!/bin/bash\n')
            for key in directives.keys():
                jdl.write("%s %s\n" % (DIRECTIVES[key],  directives[key]))
            
            work_dir = os.path.join(conf.get_work_dir_node(),job.get_work_dir())
            jdl.write('mkdir -p %s\n' % work_dir)
            jdl.write('cd %s\n' % work_dir)
            
            for d in job.get_dirs():
                jdl.write('mkdir -p %s\n' % d)
            
            jdl.write(self._get_utils(node_log_dir))           
            jdl.write(copy_files_utils)
            jdl.write(copy_files_in)
                                    
            jdl.write('%s\n' % conf.get_extra_commands_before())                 
            for task in job.get_executables():
                jdl.write(task+'\n')
                jdl.write('print_status $? "task_error"\n')
            jdl.write('%s\n' % conf.get_extra_commands_after())
            
            jdl.write(copy_files_out)                          


        # launch the job
        comm = 'qsub ' + jdl_path
        proc = subprocess.Popen(comm, stdout = subprocess.PIPE, \
        stderr = subprocess.PIPE, shell = True, close_fds = True)
        (stdoutdata, stderrdata) = proc.communicate()

        
        ret_dict['stdout']=stdoutdata
        ret_dict['stderr']=stderrdata 
        ret_dict['exitcode']=proc.returncode
        if proc.returncode != 0:
            ret_dict['job_id']='None'
            return ret_dict
        
        jobid_str = stdoutdata
        jobid = jobid_str.strip()
        ret_dict['job_id']=jobid
        
        jdl_jobid_path = os.path.join(jdl_dir,jobid+'.jdl')
        os.rename(jdl_path, jdl_jobid_path)
        j = {}
        j['jdl'] = jdl_jobid_path
        j['submit_date'] = str(datetime.datetime.now())
        j['uid'] = self._uid
        j['scheduler_log_file'] = os.path.join(host_log_dir,self._log)
        j['work_dir']=job.get_work_dir()
        state = system.State()
        state.add_job(jobid, j)
        return ret_dict
        
        
        
    
    def stat(self, job_id):
        ret_dict={'job_id':job_id}
        
        proc = subprocess.Popen('qstat ' + job_id, stdout = subprocess.PIPE, \
                                stderr = subprocess.STDOUT, shell = True, close_fds = True)
        
        
        (stdout, stderr) = proc.communicate()

        ret_dict['stdout']=stdout
        ret_dict['stderr']=stderr 
        ret_dict['exitcode']=proc.returncode        

        if proc.returncode != 0:
            return system._job_stat(job_id)

        lines = stdout.split('\n')
        for line in lines[2:]:
            args = line.split()
                            
            if args:
                status = args[4]
                if status == 'C' or status == 'E':
                    return system._job_stat(job_id)
                elif status == 'H':
                    ret_dict['status']= system.JOB_HELD
                elif status == 'Q':
                    ret_dict['status']= system.JOB_QUEUED
                elif status == 'R':
                    ret_dict['status']= system.JOB_EXECUTING
                elif status == 'T':
                    ret_dict['status']= system.JOB_PENDING
                elif status == 'W':
                    ret_dict['status']= system.JOB_PENDING
                elif status == 'S':
                    ret_dict['status']= system.JOB_SUSPENDED
                else:
                    return system._job_stat(job_id)
                return ret_dict
            
        else:           
            return system._job_stat(job_id)
    
    def delete(self, job_id):
        #TODO
        pass
    