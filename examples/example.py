# Copyright (C) 2012-2020 Euclid Science Ground Segment
#
# This library is free software; you can redistribute it and/or modify it under the terms of the GNU Lesser General
# Public License as published by the Free Software Foundation; either version 3.0 of the License, or (at your option)
# any later version.
#
# This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
# warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
# details.
#
# You should have received a copy of the GNU Lesser General Public License along with this library; if not, write to
# the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
#
import time

import euclid_ial.drm.system as ial # the only module you need to import

if __name__ == '__main__':
    work_dir = 'my_work_dir'

    # create a job object
    j = ial.Job()

    # set a working directory for the job
    j.set_work_dir(work_dir)
    # add a subdirectory do be created and used by the job
    j.add_dir('data')
    # add the directory used for the logs
    j.set_log_dir('my_log')
    # add executables
    j.add_executable('echo "foo" > data/file.txt')
    j.add_executable('sleep 5')
    j.add_executable('echo "bar" >> data/file.txt')
    # add a file name to be copied back to the submission host
    j.add_file_out('data/file.txt')
    # submit the job
    j.submit()

    # get_id() gives you the job id, when the job is submitted
    print('waiting for job: ' + j.get_id())

    # wait for the job completion
    status = ial.JOB_PENDING
    while status != ial.JOB_COMPLETED:
        time.sleep(1)
        status = j.check()
        print(status)

    #create a new job using the same working directory
    j2 = ial.Job()
    j2.set_work_dir(work_dir)
    j2.add_dir('data')
    j2.set_log_dir('my_log')
    j2.add_executable('wc data/file.txt > data/count.txt')
    # the execution node for the new job might be different
    # so you need to specify the files to be copied to the
    # execution node
    j2.add_file_in('data/file.txt')
    j2.add_file_out('data/count.txt')
    j2.submit()

    print('waiting for job: ' + j.get_id())
    
    status = ial.JOB_PENDING
    while status != ial.JOB_COMPLETED:
        time.sleep(1)
        status = j2.check()
        print(status)
    
    
    
